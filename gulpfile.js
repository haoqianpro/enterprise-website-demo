const gulp = require('gulp'),
  sass = require('gulp-sass');
  // livereload = require('gulp-livereload');


function scss(cb) {
  gulp.src('./src/scss/*.scss')
  .pipe(sass())
  .pipe(gulp.dest('./dist/css'));
  cb();
}

function css(cb) {
  gulp.src('./src/scss/*.css')
  .pipe(gulp.dest('./dist/css'));
  cb();
}

function assets(cb) {
  gulp.src('./images/*').pipe(gulp.dest('./dist/images'));
  gulp.src('./libs/**/*').pipe(gulp.dest('./dist/libs'));
  gulp.src('./videos/*').pipe(gulp.dest('./dist/videos'));
  cb();
}

function html(cb) {
  gulp.src('./src/*.html').pipe(gulp.dest('./dist/'));
  cb();
}

function js(cb) {
  gulp.src('./src/js/*.js').pipe(gulp.dest('./dist/js'));
  cb();
}

function watching() {
  gulp.watch('./src/scss/*.scss',scss);
  gulp.watch('./src/scss/*.css',css);
  gulp.watch(['./images/*','./libs/**/*','./videos/*'],assets);
  gulp.watch('./src/*.html',html);
  gulp.watch('./src/js/*.js',js);
}

exports.default = gulp.parallel(scss,css,assets,html,js);

exports.watch = watching;