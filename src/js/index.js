const mySwiper = new Swiper ('.swiper-container', {
  loop: true, // 循环模式选项
  
  // 如果需要分页器
  pagination: {
    el: '.swiper-pagination',
  },
  
  // 如果需要前进后退按钮
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
})  

//按钮
const filterBtns = document.querySelector('.filter-btns');
filterBtns.onclick = function(e) {
  const {target} = e;
  document.querySelectorAll('.filter-btn').forEach(btn => {
    btn.classList.remove('active');
  })
  target.classList.add('active');
}

//导航
const headerEl = document.querySelector('header');
const scrollTop = document.querySelector('.scroll-top');
window.addEventListener('scroll',function(){
  let height = headerEl.offsetHeight;
  if(window.pageYOffset - height > 800) {
    if(!headerEl.classList.contains('sticky')) {
      headerEl.classList.add('sticky');
    }
  }else {
    headerEl.classList.remove('sticky');
  }

  if(window.pageYOffset >= 2000) {
    scrollTop.style.display = 'block';
  }else {
    scrollTop.style.display = 'none';
  }
})

const staggeringOption = {
  delay: 300,
  distance: '50px',
  duration: 500,
  easing: 'ease-in-out',
  origin: 'bottom'
}

ScrollReveal().reveal('.feature',{...staggeringOption,interval: 350});
ScrollReveal().reveal('.service-item',{...staggeringOption,interval: 350});
ScrollReveal().reveal('.data-section',{
  beforeReveal: () => {
    anime({
      targets: '.data-piece .num',
      innerHTML: el => {
        return [0,el.innerHTML];
      },
      duration: 2000,
      round: 1,
      easing: 'easeInExpo'
    })
  }
})

//